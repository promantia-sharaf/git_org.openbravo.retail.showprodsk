/*
 ************************************************************************************
 * Copyright (C) 2014-2015 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo */

OB.UI.RenderProduct.extend({
  standardSetIdentifierContent: OB.UI.RenderProduct.prototype.setIdentifierContent,
  setIdentifierContent: function () {
    if (OB.MobileApp.model.get('permissions').OBRSPSK_ShowProdSKBrowserSearch) {
      return this.model.get('searchkey') + ' - ' + this.model.get('_identifier');
    } else {
      return this.standardSetIdentifierContent();
    }
  }
});

OB.UI.RenderOrderLine.extend({
  standardSetIdentifierContent: OB.UI.RenderOrderLine.prototype.setIdentifierContent,
  setIdentifierContent: function () {
    if (OB.MobileApp.model.get('permissions').OBRSPSK_ShowProdSKLines) {
      return this.model.get('product').get('searchkey') + ' - ' + this.model.get('product').get('_identifier');
    } else {
      return this.standardSetIdentifierContent();
    }
  }
});